<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Warisan Gajahmada</title>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=G-HEJLRF87B0"></script>
    <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());
    gtag('config', 'G-HEJLRF87B0');
    </script>

    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
    'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-TW43M8L');</script>
    <!-- End Google Tag Manager -->


    <!-- Google Fonts ============================================ -->
    <link href='https://fonts.google.com/specimen/Lora' rel='stylesheet' type='text/css'>
    <link href='https://fonts.google.com/specimen/Pop   pins' rel='stylesheet' type='text/css'>
    
    <!-- All CSS Files -->

	<!-- Bootstrap CSS
	============================================ -->
	<link rel="stylesheet" href="{{url('/')}}/css/bootstrap.min.css">

	<!-- Owl carousel CSS
	============================================ -->
	<link rel="stylesheet" href="{{url('/')}}/css/owl.carousel.css">

	<!-- Slick nav CSS
	============================================ -->
	<link rel="stylesheet" href="{{url('/')}}/css/slicknav.min.css">

	<!-- IcoFont CSS
	============================================ -->
	<link rel="stylesheet" href="{{url('/')}}/css/icofont.css">

	<!-- Lightbox CSS
	============================================ -->
	<link rel="stylesheet" href="{{url('/')}}/css/lightbox.min.css">

	<!-- Animate CSS
	============================================ -->
	<link rel="stylesheet" href="{{url('/')}}/css/animate.min.css">

	<!-- Style CSS
	============================================ -->
	<link rel="stylesheet" href="{{url('/')}}/css/style.css">

	<!-- eApp-1 CSS Files -->
	
	<link rel="stylesheet" href="{{url('/')}}/css/style-1.css">
	<link rel="stylesheet" href="{{url('/')}}/css/responsive-1.css">

</head>
<body data-spy="scroll" data-target=".header" data-offset="50">
    <!-- Google Tag Manager (noscript) -->
    <noscript>
        <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-TW43M8L" height="0" width="0" style="display:none;visibility:hidden"></iframe>
    </noscript>
    <!-- End Google Tag Manager (noscript) -->

    <!-- Page loader -->
    <div id="preloader"></div>
    @include('components.header')
    @include('components.banner')
    @include('components.about')
    <!-- overview section start -->
		<section class="overview-area ptb-80" id="caratukar"> 
			<div class="container">
				<div class="row flexbox-center">
                    <div class="col-lg-6" >
						<div class="single-overview-box">
							<img src="{{ url('/') }}/img/img-3.jpg" alt="feature" />
						</div>
					</div>
					<div class="col-lg-6">
						<div class="single-overview-box">
							<h2>Cara Tukar</h2>
							<p>Segera tukarkan koleksi Koin Gatotkaca Anda  dengan mengisi data diri dan jenis Koin Gatotkaca yang ingin ditukar</p>
							<a href="#" class="appbox-btn appbox-btn3">Tukar Koin</a>
						</div>
					</div>
				</div>
			</div>
        </section><!-- overview section end -->
    
        <!-- download section start -->
		<section class="download-area ptb-100" id="order">
			<div class="container">
				<div class="row flexbox-center">
					<div class="col-lg-6">
						<div class="single-download-box">
							<h1>Ayo, Tunggu Apa Lagi?</h1>
							<p>Segera kumpulkan Koin Gatotkaca dan menangi hadiah jutaan rupiah! Dapatkan Koin Gatotkaca melalui pembelian Bundle Isi 6 pcs Larutan Penyegar Cap Badak Edisi Gatotkaca.</p>
							<a href="#" class="appbox-btn"> Tokopedia</a>
							<a href="#" class="appbox-btn"> Shopee</a>
							<a href="#" class="appbox-btn"> Blibli</a>
						</div>
                    </div>
                    <div class="col-lg-6">
						<div class="single-overview-box">
							<img src="{{ url('/') }}/img/img-4.jpg" alt="feature" />
						</div>
					</div>
				</div>
			</div>
		</section><!-- download section end -->
		
	@include('components.footer')
	
    <a href="#" class="scrolltotop">
        <i class="icofont icofont-arrow-up" aria-hidden="true"></i>
    </a>

    <!-- All JS Files-->
	<!-- jQuery
	============================================ -->
	<script src="{{url('/')}}/js/jquery.min.js"></script>

	<!-- Bootstrap JS
	============================================ -->
	<script src="{{url('/')}}/js/bootstrap.min.js"></script>

	<!-- Slick nav JS
	============================================ -->
	<script src="{{url('/')}}/js/jquery.slicknav.min.js"></script>

	<!-- Owl carousel JS
	============================================ -->
	<script src="{{url('/')}}/js/owl.carousel.min.js"></script>

	<!-- EasyPieChart JS
	============================================ -->
	<script src="{{url('/')}}/js/jquery.easypiechart.min.js"></script>

	<!-- Lightbox JS ============================================ -->
	<script src="{{url('/')}}/js/lightbox.min.js"></script>

	<!-- Common JS ============================================ -->
	<script src="{{url('/')}}/js/common.js"></script>

	<!-- Main JS ============================================ -->
	<script src="{{url('/')}}/js/main-1.js"></script>
	<script src="{{url('/')}}/js/main-2.js"></script>
	<script src="{{url('/')}}/js/main-3.js"></script>
	<script src="{{url('/')}}/js/main-4.js"></script>
	<script src="{{url('/')}}/js/main-5.js"></script>
</body>
</html>