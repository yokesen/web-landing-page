<!-- banner section start -->
<section class="banner-area" id="home">
    <div class="container">
        <div class="row flexbox-center">
            <div class="col-lg-7">
                <div class="single-banner">
                    <h1>Kumpulin Koin Gatotkaca Bisa Dapat Hadiah Jutaan Rupiah?</h1>
                    <p>Ayo kumpulkan Koin Gatotkaca sebanyak-banyaknya! Mulai dari Koin Reguler hingga Koin Ultra Premium, semuanya bisa ditukarkan dengan hadiah jutaan rupiah!</p>
                    <a href="#koin" class="appbox-btn">Cek Sekarang</a>
                </div>
            </div>
            <div class="col-lg-5">
                <div class="single-banner text-lg-left text-center">
                    <img src="{{url('/')}}/img/header.png" alt="banner" />
                </div>
            </div>
        </div>
    </div>
</section><!-- banner section end -->