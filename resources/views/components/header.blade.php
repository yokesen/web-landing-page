<!-- header section start -->
<header class="header">
    <div class="container">
        <div class="row">
            <div class="col-md-2 col-6">
                <div class="logo">
                    {{-- <a href="#home"><img src="{{url('/')}}/img/wgm.png" alt="logo" /></a> --}}
                    <a href="#home" style="color: white; font-size: 20px">Warisan Gajahmada</a>
                </div>
            </div>
            <div class="col-md-10 col-6">
                <div class="responsive-menu"></div>
                <div class="mainmenu">
                    <ul id="primary-menu">
                        <li><a class="nav-link active" href="#home">Home</a></li>
                        <li><a class="nav-link" href="#koin">Koin Gatotkaca</a></li>
                        <li><a class="nav-link" href="#fungsi">Fungsi</a></li>
                        <li><a class="nav-link" href="#caratukar">Cara Tukar</a></li>
                        <li><a class="nav-link" href="#order">Order</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</header><!-- header section end -->